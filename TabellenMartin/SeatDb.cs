using Microsoft.EntityFrameworkCore;
using TabellenMartin.Models;

namespace TabellenMartin
{
    public class SeatDb : DbContext
    {

        public SeatDb(DbContextOptions o) : base(o)
        {
            
        }
        
        public DbSet<ItemType> ItemTypes { get; set; }
        
        public DbSet<CharacterBlueprint> CharacterBlueprints { get; set; }
        public DbSet<CharacterInfo> CharacterInfos { get; set; }
        public DbSet<Structure> Structures { get; set; }
        public DbSet<Planet> Planets { get; set; }
        public DbSet<SeatUser> Users { get; set; }
        public DbSet<SquadMember> SquadMembers { get; set; }
    }
}