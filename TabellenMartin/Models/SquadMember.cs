using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace TabellenMartin.Models
{
    [Table("squad_member"), Keyless]
    public class SquadMember
    {
        [Column("squad_id")]
        public int SquadId { get; set; }

        [Column("user_id")]
        public int UserId { get; set; }
    }
}