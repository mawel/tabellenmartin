using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.InteropServices;

namespace TabellenMartin.Models
{
    [Table("invTypes")]
    public class ItemType
    {
        [Key, Column("typeID")]
        public int Id { get; set; }

        [Column("typeName")]
        public string Name { get; set; }
        
        
    }
}