using System.Collections.Generic;
using System.Text;
using ConsoleTables;

namespace TabellenMartin
{
    public static class LookupResultFormatter
    {
        public static string FormatLookupResults(LookupResult[] results, int page = 1, int totalPages = 1,
            bool header = true)
        {
            if (results.Length == 0)
                return "";

            var sb = new StringBuilder();

            if (header)
            {
                sb.AppendLine(results[0].BlueprintName);

                // if (totalPages > 1)
                // {
                //     sb.AppendLine($"\t({page}/{totalPages})");
                // }
                // else
                // {
                //     sb.AppendLine();
                // }

                // sb.AppendLine("```");
                sb.AppendLine("Character: ME/TE/Runs");
                sb.AppendLine();
            }

            foreach (var rs in results)
            {
                // sb.AppendFormat("{0}: ME={1} TE={2} Runs={3} Location={4}", rs.CharacterName.PadRight(15), rs.MaterialEfficiency.ToString().PadRight(2), rs.TimeEfficiency.ToString().PadRight(2), rs.NoOfRuns.ToString().PadRight(2), rs.Location);
                sb.AppendFormat("{0}: {1}/{2}/{3}", rs.CharacterName, rs.MaterialEfficiency, rs.TimeEfficiency,
                    rs.NoOfRuns, rs.Location);
                sb.AppendLine();
            }

            // sb.AppendLine("```");

            return sb.ToString();
        }
    }
}
