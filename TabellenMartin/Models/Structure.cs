using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TabellenMartin.Models
{
    [Table("universe_structures")]
    public class Structure
    {
        [Column("structure_id")]
        public UInt64 Id { get; set; }

        [Column("name")]
        public string Name { get; set; }
    }
}