using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TabellenMartin.Models
{
    [Table("planets")]
    public class Planet
    {
        [Column("planet_id")]
        public UInt64 Id { get; set; }

        [Column("name")]
        public string Name { get; set; }
    }
}