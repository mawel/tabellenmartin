namespace TabellenMartin
{
    public class BotConfig
    {
        public string ApplicationName { get; set; }
        public string NoResultsMessage { get; set; }
        public string DiscordBotToken { get; set; }
        
        public bool RespondInPrivateChat { get; set; }
        
        public string DatabaseConnectionString { get; set; }

        public int MaxResultsPerMessage { get; set; } = 10;
        public int MaxResults { get; set; } = 30;

        public int SquadId { get; set; }
    }
}