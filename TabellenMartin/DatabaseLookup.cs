using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TabellenMartin.Models;

namespace TabellenMartin
{
    public class DatabaseLookup : ILookup
    {
        private SeatDb _db;
        private BotConfig _cfg;
        public DatabaseLookup(SeatDb db, IOptions<BotConfig> cfg)
        {
            _db = db;
            _cfg = cfg.Value;
        }
        
        public async Task<LookupResult[]> Lookup(string searchName)
        {
            searchName = searchName.Replace(" Blueprint", "", StringComparison.OrdinalIgnoreCase).Trim();
            
            var itemType = await _db.ItemTypes.AsQueryable()
                .FirstOrDefaultAsync(e => EF.Functions.Like(e.Name, $"%{searchName}% Blueprint"));

            if (itemType == null)
                return Array.Empty<LookupResult>();
                
            var rs = _db.CharacterBlueprints.AsQueryable()
                .Where(e => e.ItemType == itemType)
                .Include(e=>e.CharacterInfo)
                .Where(e => _db.SquadMembers.Any(s => s.SquadId == _cfg.SquadId 
                                            && s.UserId == _db.Users.AsQueryable().Single(k => k.MainCharacter == e.CharacterInfo).Id) 
                    )
                .Select(e => new LookupResult()
                {
                    BlueprintName = itemType.Name,
                    CharacterName = e.CharacterInfo.Name,
                    MaterialEfficiency = e.MaterialEfficiency,
                    TimeEfficiency = e.TimeEfficiency,
                    NoOfRuns = e.Runs,
                    Location = 
                               _db.Structures.FirstOrDefault(a => a.Id == e.LocationId).Name
                               ?? _db.Planets.FirstOrDefault(a => a.Id == e.LocationId).Name
                               ?? "Unknown"//$"Unknown ({e.LocationFlag}, Id={e.LocationId})"
                })
                .OrderBy(e => e.CharacterName)
                .Take(_cfg.MaxResults)
                ;

            return await rs.ToArrayAsync();
        }
    }
}