using System;
using System.Configuration;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace TabellenMartin
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Reading configuration...");
            
            var configuration = new ConfigurationBuilder()
                    .AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "config.json"), optional: false)
                    .Build();

            var sc = new ServiceCollection();

            sc.AddOptions();
            sc.AddLogging(b => b
                .AddConfiguration(configuration.GetSection("Logging"))
                .AddConsole()
            );
            

            sc.Configure<BotConfig>(configuration.GetSection("Bot"));
            
            
            sc.AddDbContext<SeatDb>(o =>
            {
                o.UseMySql(configuration.GetConnectionString("seat"), new MariaDbServerVersion(new Version(10, 5, 5)));
            });

            sc.AddSingleton<ILookup, DatabaseLookup>();
            sc.AddSingleton<DiscordIO>();
            
            await using var services = sc.BuildServiceProvider();
            
            await services.GetRequiredService<DiscordIO>()
                .StartAsync();
            
            await Task.Delay(Timeout.Infinite);
        }
        
    }
}
