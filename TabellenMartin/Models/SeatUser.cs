using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace TabellenMartin.Models
{
    [Table("users")]
    public class SeatUser
    {
        [Key,Column("id")]
        public int Id { get; set; }

        [Column("main_character_id")]
        public UInt64 MainCharacterId { get; set; }
        
        [ForeignKey("MainCharacterId")]
        public CharacterInfo MainCharacter { get; set; }

    }
}