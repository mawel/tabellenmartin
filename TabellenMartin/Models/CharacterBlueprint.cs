using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TabellenMartin.Models
{
    [Table("character_blueprints")]
    public class CharacterBlueprint
    {
        [Key, Column("item_id")]
        public long Id { get; set; }
        
        [Column("character_id")]
        public UInt64 CharacterId { get; set; }
        [ForeignKey("CharacterId")]
        public CharacterInfo CharacterInfo { get; set; }
        
        [Column("location_id")]
        public UInt64 LocationId { get; set; }
        
        [Column("location_flag")]
        public string LocationFlag { get; set; }

        [Column("quantity")]
        public int Quantity { get; set; }
        
        [Column("time_efficiency")]
        public int TimeEfficiency { get; set; }
        
        [Column("material_efficiency")]
        public int MaterialEfficiency { get; set; }
        
        [Column("runs")]
        public int Runs { get; set; }

        [ForeignKey("type_id")]
        public ItemType ItemType { get; set; }
    }
}