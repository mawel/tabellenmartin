using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TabellenMartin.Models
{
    [Table("character_infos")]
    public class CharacterInfo
    {
        [Key, Column("character_id")]
        public UInt64 CharacterId { get; set; }

        [Column("name")]
        public string Name { get; set; }
    }
}