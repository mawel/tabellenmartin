using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MoreLinq;

namespace TabellenMartin
{
    public class DiscordIO
    {
        private DiscordSocketClient _client;
        private ILookup _lookup;
        private BotConfig _cfg;
        private ILogger _logger;
        public DiscordIO(IOptions<BotConfig> cfg, ILookup lookup, ILogger<DiscordIO> logger)
        {
            _cfg = cfg.Value;
            _lookup = lookup;
            _logger = logger;
            
            _client = new DiscordSocketClient(new DiscordSocketConfig());
            _client.MessageReceived += OnMessageReceived;
        }

        public async Task StartAsync()
        {
            
            _logger.LogInformation("Connecting to Discord...");
            await _client.LoginAsync(TokenType.Bot, _cfg.DiscordBotToken);
            await _client.StartAsync();
            
            _logger.LogInformation("Connected");
        }

        private async Task OnMessageReceived(SocketMessage msg)
        {
            try
            {
                if(msg.Source != MessageSource.User)
                    return;

                _logger.LogTrace("Processing message: {Content}", msg.Content);

                if (msg.Content.Trim().StartsWith("🤖 🍆 😏"))
                {
                    await msg.Author.SendMessageAsync("beep beep boop :smirk:");
                    return;
                }

                var rs = await _lookup.Lookup(msg.Content);
                if (rs.Length > 0)
                {
                    var totalPages = rs.Length / _cfg.MaxResultsPerMessage + 1;
                    var page = 1;
                    foreach (var b in rs.Batch(_cfg.MaxResultsPerMessage))
                    {
                        await Respond(msg, LookupResultFormatter.FormatLookupResults(b.ToArray(), page, totalPages, page == 1));
                        page++;
                    }
                }
                else
                {
                    await Respond(msg, _cfg.NoResultsMessage);
                }

                
                
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "Error while handling message: {Content}", msg.Content);
                Console.WriteLine(e);
            }
        }

        private Task Respond(SocketMessage msg, string response)
        {
            if (_cfg.RespondInPrivateChat)
            {
                return msg.Author.SendMessageAsync(response);
            }
            else
            {
                return msg.Channel.SendMessageAsync(response);                
            }
        }
    }
}
