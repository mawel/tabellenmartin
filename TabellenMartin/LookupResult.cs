namespace TabellenMartin
{
    public class LookupResult
    {
        public string CharacterName { get; set; }
        public int NoOfRuns { get; set; }
        public int MaterialEfficiency { get; set; }
        public int TimeEfficiency { get; set; }
        public string Location { get; set; }
        public string BlueprintName { get; set; }
    }
}