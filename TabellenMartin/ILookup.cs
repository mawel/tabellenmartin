using System.Threading.Tasks;

namespace TabellenMartin
{
    public interface ILookup
    {
        Task<LookupResult[]> Lookup(string searchName);
    }
}